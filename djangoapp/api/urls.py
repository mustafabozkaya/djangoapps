# This is the simplest view possible in Django. To call the view, we need to map it to a URL - and for this we need a URLconf.

# In api/urls.py:
# path is a function that takes a regex and a view and returns a URLconf object
from pathlib import Path
from django.urls import path
from . import views  # import views from the current directory

urlpatterns = [
    # The regex is the URL that we want to map to the view
    # path(route, view, kwargs, name) - route is a regex, view is a view, kwargs is a dictionary of keyword arguments and name is a name for the URL
    path('todo/', views.TodoList.as_view()),
    path('todo/create/', views.TodoCreate.as_view()),
    path('todo/<int:pk>/', views.TodoRetriveUpdateDestroy.as_view()),
    # this is the name of the URL
    path("todo/<int:pk>/complete/", views.TodoToggleComplate.as_view()),
    path("register/", views.register, name="register"),
    path("login/", views.login, name="login"),


]
