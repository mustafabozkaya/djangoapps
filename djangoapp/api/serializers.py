# serializers used to serialize data to JSON,it is used to convert python data to JSON
from rest_framework import serializers
from todo.models import Todo  # import the model from the todo app


class TodoSerializer(serializers.ModelSerializer):
    # ModelSerializer is a serializer that can be used to serialize a queryset or single object.
    # it is used to serialize the Todo model
    class Meta:
        model = Todo  # set the model to the Todo model
        # set the fields to the fields of the Todo model
        fields = ('id', 'title', 'description',
                  'created_at', 'completed', 'updated_at')
        # set the read_only_fields to the read_only_fields of the Todo model
        read_only_fields = ('id', 'created_at', "completed", 'updated_at')
        # set the extra_kwargs to the extra_kwargs of the Todo model
        extra_kwargs = {
            'title': {'required': True},
            'description': {'required': True}
        }

# create a serializer class that inherits from the ModelSerializer class


class TodoUpdateSerializer(serializers.ModelSerializer):
    # set the created_at field to read only
    # created_at = serializers.ReadOnlyField()
    # completed = serializers.ReadOnlyField()  # set the completed field to read only
    # # set the updated_at field to read only
    # updated_at = serializers.ReadOnlyField()

    class Meta:  # create a meta class that inherits from the Meta class
        model = Todo  # set the model to the Todo model
        # set the fields to the fields that we want to serialize
        fields = ('id', 'title', 'description',
                  'created_at', 'completed', 'updated_at')
        # fields_read_only = ("id", 'created_at', 'completed', 'updated_at')


class TodoComplateSerializer(serializers.ModelSerializer):
    completed = serializers.BooleanField()  # set the completed field to read only

    class Meta:
        model = Todo
        # fields means the fields that we want to serialize for the model(write the fields that we want to serialize,read the fields that we want to deserialize)
        fields = ('id', "completed")
        read_only_fields = ('title', 'description',
                            'created_at', 'updated_at')  # fields_only_read means that we only want to serialize the fields that we want to read
        # fields_only_write means that we only want to deserialize the fields that we want to write
