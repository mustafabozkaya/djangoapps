# generic is used to create a view that can be used to create, update, delete, and retrieve data from a database using RESTful
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from rest_framework import generics, permissions
from todo.models import Todo
# import the serializer from the serializers app
from .serializers import TodoSerializer, TodoUpdateSerializer, TodoComplateSerializer
# import the permissions class from the rest_framework app
from rest_framework import permissions
# it is used to get the user from the request
from django.contrib.auth.models import User
# it is used to parse the data from the request

# for token authentication
# import the IntegrityError from the django.db app,it is used to handle the IntegrityError exception,intehrity error is raised when we try to insert duplicate data
from django.db import IntegrityError
from rest_framework.parsers import JSONParser
from rest_framework.authtoken.models import Token
from django.http import JsonResponse
# it is used to disable the csrf protection for the view function ,CSRF stands for Cross Site Request Forgery
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout

# Create register view


@csrf_exempt
def register(request):
    if request.method == "POST":
        # get the data from the request
        print("*" * 50)
        print("request: ", request)
        print("-" * 50)
        data = JSONParser().parse(request)  # parse the data from the request
        print("data: ", data)
        print("-" * 50)
        # create a user object
        user = User()
        # set the username and password of the user
        user.username = data["username"]
        user.email = data["email"]
        user.set_password(data["password"])

        try:
            # save the user
            user.save()
            # create a token for the user
            token = Token.objects.create(user=user)
            # return the token to the user
            return JsonResponse({"token": token.key}, status=201)
        except IntegrityError:
            # return an error message to the user
            return JsonResponse({"error": "That username has already been taken. Please choose a new username."}, status=400)


# Create login view
@csrf_exempt
def login(request):
    if request.method == "POST":
        # get the data from the request
        data = JSONParser().parse(request)
        # authenticate the user
        user = authenticate(
            username=data["username"], password=data["password"])
        # check if the user is authenticated
        if user is not None:
            try:  # try to get the token of the user
                token = Token.objects.get(user=user)

            except Token.DoesNotExist:  # if the token does not exist,create a new token for the user
                token = Token.objects.create(user=user)
            # return the token to the user
            return JsonResponse({f" user {user.username} is logged in": token.key}, status=200)
        else:
            # return an error message to the user
            return JsonResponse({"error": "Invalid username and/or password."}, status=400)
# create a class that inherits from the ListAPIView class


class TodoList(generics.ListAPIView):
    # ListAPIView is a generic view that provides default behavior for listing data in a format that is specified by the serializer.
    queryset = Todo.objects  # set the queryset to the Todo model
    # set the serializer to the TodoSerializer class
    serializer_class = TodoSerializer
    # set the permission to the IsAuthenticatedOrReadOnly class
    permission_classes = [permissions.IsAuthenticated]

    # create a get_queryset method that overrides the get_queryset method from the ListAPIView class
    def get_queryset(self):
        user = self.request.user  # get the user from the request object
        # return the queryset filtered by the user and ordered by the created_at field in descending order
        return self.queryset.filter(user=user).order_by('-created_at')


class TodoCreate(generics.ListCreateAPIView):
    # CreateAPIView is a generic view that provides default behavior for creating a new object.
    queryset = Todo.objects  # set the queryset to the Todo model
    # set the serializer to the TodoSerializer class
    serializer_class = TodoSerializer
    # set the permission to the IsAuthenticated class
    permission_classes = [permissions.IsAuthenticated]
    # create a get_queryset method that overrides the get_queryset method from the CreateAPIView class

    def get_queryset(self):
        user = self.request.user  # get the user from the request object
        # return the queryset filtered by the user and ordered by the created_at field in descending order
        return self.queryset.filter(user=user).order_by('-created_at')
    # create a perform_create method that overrides the perform_create method from the CreateAPIView class

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

# create a class that inherits from the RetrieveUpdateDestroyAPIView class


class TodoRetriveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    # RetrieveUpdateDestroyAPIView is a generic view that provides default behavior for retrieving, updating, and deleting an object.
    queryset = Todo.objects  # set the queryset to the Todo model
    # set the serializer to the TodoSerializer class
    serializer_class = TodoUpdateSerializer
    # set the permission to the IsAuthenticatedOrReadOnly class
    permission_classes = [permissions.IsAuthenticated]
    # create a get_queryset method that overrides the get_queryset method from the RetrieveUpdateDestroyAPIView class

    def get_queryset(self):
        user = self.request.user  # get the user from the request object
        # user  can only update and delete their own todos
        return self.queryset.filter(user=user)
    # create a perform_update method that overrides the perform_update method from the RetrieveUpdateDestroyAPIView class

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    # create a perform_destroy method that overrides the perform_destroy method from the RetrieveUpdateDestroyAPIView class

    def perform_destroy(self, instance):
        instance.delete()


class TodoToggleComplate(generics.UpdateAPIView):
    queryset = Todo.objects  # set the queryset to the Todo model
    serializer_class = TodoComplateSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user  # get the user from the request object
        return self.queryset.filter(user=user)

    def perform_update(self, serializer):

        # toggle the completed field of the todo,save the todo and return the todo
        serializer.save(completed=not serializer.instance.completed)
