from cgi import test
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from .models import Question, Choice

# Create your tests here.

class QuestionModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        test_question=Question.objects.create(question_text="What's new?", pub_date='2021-10-05')

    def test_question_text_content(self):
        question=Question.objects.get(id=1)
        expected_object_name=f'{question.question_text}'
        self.assertEqual(expected_object_name,"What's new?")
        self.assertEqual(str(question), "What's new?")

class ChoiceModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        test_question=Question.objects.create(question_text="What's new?", pub_date='2021-10-05')
        test_choice=Choice.objects.create(choice_text="Not much", question=test_question, votes=0)
       

    def test_choice_text_content(self):
        choice=Choice.objects.get(id=1)
        expected_object_name=f'{choice.choice_text}'
        self.assertEqual(expected_object_name,"Not much")
        self.assertEqual(str(choice), "Not much")
