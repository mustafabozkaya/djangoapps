# This is the simplest view possible in Django. To call the view, we need to map it to a URL - and for this we need a URLconf.

# In polls/urls.py:
from django.urls import path # path is a function that takes a regex and a view and returns a URLconf object
from . import views # import views from the current directory

urlpatterns = [
    # The regex is the URL that we want to map to the view
    # path(route, view, kwargs, name) - route is a regex, view is a view, kwargs is a dictionary of keyword arguments and name is a name for the URL
    path('', views.index, name='index'),
    path('<int:question_id>/', views.detail, name='detail'),
    path('<int:question_id>/results/', views.results, name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]