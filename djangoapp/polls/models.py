from django.db import models
import datetime
from django.utils import timezone # timezone is a module that contains functions that help us work with timezones


# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200) # CharField is a field that stores a string of fixed length
    pub_date = models.DateTimeField('date published') # DateTimeField is a field that stores a date and time

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE) # ForeignKey is a field that stores a one-to-one relationship with Question model
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
    
