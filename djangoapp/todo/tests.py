from django.test import TestCase
from todo.models import Todo
from django.contrib.auth.models import User



# Create your tests here.
class TodoModelTest(TestCase):
    @classmethod # class method is used to create a class method,it means that the method is bound to the class and not the object of the class
    def setUpTestData(cls): # cls is used to refer to the class
        # Set up non-modified objects used by all test methods
        test_user = User.objects.create_user(
            username='testuser',
            password='12345'
        )
        test_todoobject=Todo.objects.create(
            id=1,
            title='test title',
            description='test description',
            completed=True,
            user=test_user
        )
     

    def test_title_content(self):
        todo=Todo.objects.get(id=1)
        expected_obj_title=f'{todo.title}'
        expected_obj_user=f'{todo.user}'
        expected_obj_description=f'{todo.description}'
        expected_obj_completed=f'{todo.completed}'
        self.assertEqual(expected_obj_title,'test title') # assertEqual is used to check if the expected value is equal to the actual value
        self.assertEqual(expected_obj_user,'testuser')
        self.assertEqual(expected_obj_description,'test description')
        self.assertEqual(expected_obj_completed,'True')
        # test todos reruns string representation
        self.assertEqual(str(todo), 'test title - test description - True - testuser')
        