from unicodedata import category
from django.db import models
# import the User model from the django auth app
from django.contrib.auth.models import User


# Create your models here.
class Category(models.Model):
    # create a name field that is a CharField with max_length of 100
    name = models.CharField(max_length=100,default="planning")
    # create a slug field that is a SlugField with max_length of 100
    slug = models.SlugField(max_length=100)
    # create a created_at field that is a DateTimeField with auto_now_add set to True
    created_at = models.DateTimeField(auto_now_add=True)
    # create a updated_at field that is a DateTimeField with auto_now set to True
    updated_at = models.DateTimeField(auto_now=True)

    # create a class Meta that sets the verbose_name_plural to categories
    class Meta:
        verbose_name_plural = 'categories'

    # create a __str__ method that returns the name of the category
    def __str__(self):
        return self.name
class Todo(models.Model):
    title = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE,default=1) # default=1 is a temporary fix for the error:
    # PROTECT prevents the user from being deleted if they have a todo category
    description = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    completed = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)

    # user who created the todo
    # on_delete=models.CASCADE means that if the user is deleted, the todo is deleted as well its related to the user one to one relationship
    # the other option is on_delete=models.PROTECT which means that the todo will not be deleted if the user is deleted
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title + ' - ' + self.description + ' - ' + str(self.completed) + ' - ' + str(self.user.username)