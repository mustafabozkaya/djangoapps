import { Component } from "react";
import {
  Navbar,
  Nav,
  NavDropdown,
  Form,
  FormControl,
  Offcanvas,
  Container,
  Button,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useState } from "react";

const Header = () => {
  const [user, setUser] = useState(null);

  const [expand, setExpand] = useState("sm");
  const [token, setToken] = useState(null);
  const [error, setError] = useState("");

  return (
    <>
      <Navbar bg="primary" variant="dark" expand={expand} className="mb-3">
        <Container fluid>
          <Navbar.Brand href="#">ToDo&APP</Navbar.Brand>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
          <Navbar.Offcanvas
            id={`offcanvasNavbar-expand-${expand}`}
            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
            placement="end"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                Offcanvas
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <Nav.Link href="#">
                  <Link className="nav-link" to="{/todo}">
                    TodoList
                  </Link>
                </Nav.Link>

                {user ? (
                  <Nav.Link href="#">
                    <Link className="nav-link">Logout [{user.name}]</Link>
                  </Nav.Link>
                ) : (
                  <>
                    <Nav.Link href="#">
                      <Link className="nav-link" to="{/login}">
                        Login
                      </Link>
                    </Nav.Link>
                    <Nav.Link href="#">
                      <Link className="nav-link" to="{/signup}">
                        Register
                      </Link>
                    </Nav.Link>
                  </>
                )}
                {/* <Nav.Link href="#action1">Home</Nav.Link>
                    <Nav.Link href="#action2">Link</Nav.Link> */}
                {/* <NavDropdown
                      title="Dropdown"
                      id={`offcanvasNavbarDropdown-expand-${expand}`}
                    >
                      <NavDropdown.Item href="#action3">
                        Action
                      </NavDropdown.Item>
                      <NavDropdown.Item href="#action4">
                        Another action
                      </NavDropdown.Item>
                      <NavDropdown.Divider />
                      <NavDropdown.Item href="#action5">
                        Something else here
                      </NavDropdown.Item>
                    </NavDropdown> */}
              </Nav>
              <Form className="d-flex">
                <Form.Control
                  type="search"
                  placeholder="Search"
                  className="me-2"
                  aria-label="Search"
                />
                <Button variant="outline-success">Search</Button>
              </Form>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>
    </>
  );
};
export default Header;
