import logo from "./logo.svg";
import { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from "./components/Home";
import Header from "./components/Navbar";
import Footer from "./components/Footer";
import Sidebar from "./components/Sidebar";
import About from "./components/About";
import Contact from "./components/Contact";
import Login from "./components/Login";
import Register from "./components/Register";
import Todolist from "./components/Todolist";
import TodoEdit from "./components/TodoEdit";
import { useState } from "react";

import "./App.css";
import alertfy from "alertifyjs";

function App() {
  return (
    <div className="App">
      <Header />
    </div>
  );
}

export default App;
